//
//  PokemonViewController.swift
//  Pokedex
//
//  Created by Dario Herrera on 20/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class PokemonViewController: UIViewController {
    
    var peso : Int = 0
    var altura : Int = 0
    var id : Int = 0
    
    @IBOutlet weak var imagenPokemon: UIImageView!
    
    @IBOutlet weak var weightPokemon: UILabel!
    
    @IBOutlet weak var heightPokemon: UILabel!
    
    
    @IBOutlet weak var idPokemon: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mostrarDatos()
        
        

        // Do any additional setup after loading the view.
    }
    
    func mostrarDatos() {
        weightPokemon.text = String(peso)
        heightPokemon.text = String(altura)
        idPokemon.text = String(id)
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                
                self.imagenPokemon.image = image
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
