//
//  ViewController.swift
//  Pokedex
//
//  Created by Dario Herrera on 13/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var pokemon:[Pokemon] = []
    
    var pesoO : Int = 0
    var alturaA : Int = 0
    var idD : Int = 0
    
    @IBOutlet weak var pokemonTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let network = Network()
        network.getAllPokemon{ (pokemonArray) in
            self.pokemon = pokemonArray
            self.pokemonTableView.reloadData()
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "info" {
            let segundaVista = segue.destination as? PokemonViewController
            segundaVista?.peso = pesoO
            segundaVista?.altura = alturaA
            segundaVista?.id = idD
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "info", sender: self)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        pesoO =  pokemon[indexPath.row].height
        alturaA = pokemon[indexPath.row].weight
        idD = pokemon[indexPath.row].id
    
        return cell
        
    }
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
 
    


}

