//
//  Pokemon.swift
//  Pokedex
//
//  Created by Dario Herrera on 13/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import Foundation

struct Pokemon: Decodable {
    
    var name:String
    var weight:Int
    var height:Int
    var sprites:Sprite
    
    var id:Int
}

struct Sprite:Decodable {
    
    var defaultSprite:String
    
    enum CodingKeys: String, CodingKey {
        
        case defaultSprite = "front_default"
    }
}
